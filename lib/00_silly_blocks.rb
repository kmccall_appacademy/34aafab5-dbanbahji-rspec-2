def reverser
  yield.split.map { |word| word.reverse }.join(" ")
end


def adder(num = 1)
  num + yield
end

def repeater(repeats = 1)
  repeats.times{ yield }
end
