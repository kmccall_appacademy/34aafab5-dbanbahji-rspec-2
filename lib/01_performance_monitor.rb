def measure(n = 1)
  counter = n
  total = 0
  while counter > 0
    start = Time.now
    counter -= 1
    yield
    stop = Time.now
    total += stop - start
  end
  total / n
end
